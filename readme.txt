Base app for staring a new Java Web App project

Contains:
-project directory structure
-Maven pom.xml
-.gitignore
-Bootstrap CSS
-Font Awesome
-jQuery
-jQueryUI
-Angular

Backlog:
-Cargo deployment
-HTML reuse for common areas (i.e. page headers)


Completed:
-Filters, with URL patterns
-JSPs are intercepted, and redirected back to root
-Maven profiles
-Update Runtime.config with Maven Filter (i.e html or java with email address)
-Log4J, with root appender
-version part of maven build
-Constants pattern
-jUnit test
-DB Connect/Transaction pattern
-DB Pool (BoneCP for custom SQL)
-REST Controllers with Jersey
-JSON with Jackson
-turn off directory listings (with index.html)
-Cache busting automatic, release level
-Hibernate (with C3P0)
-REST assured
-Authentication for AppUser




