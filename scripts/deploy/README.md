Use for deploying war files to servers

Install python 2.7, or equivalent 
Install pip, with 'get-pip.py'
Install paramiko, with 'pip install paramiko', more at http://www.paramiko.org/
Install dependency pycrypto, with pycrypto-2.6.win32-py2.7.exe binary, from http://www.voidspace.org.uk/python/modules.shtml#pycrypto
Install dependency ecdsa, with 'pip install ecdsa'
