#Here are the constants for the program

#Default Values
HOST="hpub"
USER="sagan"
PASSWORD="unknown"
FILE_NAME="war.txt"
LOCAL_DIR=r"C:\Users\Sagan\Documents\github\baseApp\scripts\deploy\test\\"
REMOTE_DIR=r"/home/sagan/"
DEPLOY_DIR=r"/var/lib/tomcat6/vhost_baseapp/"

TARGET_HOSTS={
    'qa' : {
        'host' : 'hpub',
        'user' : 'sagan',
        'password' : None,
        'file_name' : 'war.txt',
        'local_dir' : r'C:\Users\Sagan\Documents\github\baseApp\scripts\deploy\test\\',
        'remote_dir' : '/home/sagan/',
        'deploy_dir' : '/var/lib/tomcat6/vhost_baseapp/'
    },
    'prod' : {
        'host' : 'steplify.com',
        'user' : 'ubuntu',
        'password' : None,
        'file_name' : 'war.txt',
        'local_dir' : r'C:\Users\Sagan\Documents\github\baseApp\scripts\deploy\test\\',
        'remote_dir' : '/home/sagan/',
        'deploy_dir' : '/var/lib/tomcat6/vhost_baseapp/'
    }
}