import sys

#Main function starts here
def main():
    print "This line will be printed1."
    print "This line will be printed2."
    print "This line will be printed3."

    #Collect user input
    print "What do you have to say for yourself?"
    data = sys.stdin.readline()
    data = data.rstrip()
    print "start-" + data + "-end"


#For unit testing
def getOne():
    return 1;


#Kick off the script; during a unit test this wont get run (__name__ is 'hello'
if __name__ == '__main__':
    main()
