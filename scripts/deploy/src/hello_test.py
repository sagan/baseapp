import unittest
import hello

# Here's our "unit tests".
class IsOddTests(unittest.TestCase):

    def testOne(self):
        self.failUnless(hello.getOne() == 1)

    def testTwo(self):
        self.failIf(hello.getOne() == 2)
