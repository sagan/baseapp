import sys
import AppConstants
import paramiko


#Main function starts here
def main():
    (sHost, sFile, sUser, sPassword) = get_user_inputs()
    print "HOST will be: " + sHost
    print "FILE will be: " + sFile
    print "SSH-USER will be: " + sUser
    print "SSH-PASSWORD will be: " + sPassword
    print ""
    print "Ready, Set, Press enter to GO..."
    sys.stdin.readline()
    #transfer_file_now(sHost, sFile, sUser, sPassword)
    deploy_file_now(sHost, sFile, sUser, sPassword)


#Consile interaction
def get_user_inputs():
    #Get user input
    print "Enter remote HOST (default is '" + AppConstants.HOST + "'):"
    sHost = sys.stdin.readline().rstrip()
    if (sHost == ''):
        sHost = AppConstants.HOST

    print "Enter transfer FILENAME (default is '" + AppConstants.FILE_NAME + "' in " + AppConstants.LOCAL_DIR + "):"
    sFile = sys.stdin.readline().rstrip()
    if (sFile == ''):
        sFile = AppConstants.FILE_NAME

    print "Enter SSH USER (default is '" + AppConstants.USER + "'):"
    sUser = sys.stdin.readline().rstrip()
    if (sUser == ''):
        sUser = AppConstants.USER

    print "Enter SSH PASSWORD (default is '" + AppConstants.PASSWORD + "'):"
    sPassword = sys.stdin.readline().rstrip()
    if (sPassword == ''):
        sPassword = AppConstants.PASSWORD

    return (sHost, sFile, sUser, sPassword)


#Use paramiko lib for SCP
def transfer_file_now(sHost, sFile, sUser, sPassword):
    print "Starting SCP file transfer..."
    t = paramiko.Transport((sHost, 22))
    t.connect(username=sUser, password=sPassword)
    sftp = paramiko.SFTPClient.from_transport(t)
    sftp.put(AppConstants.LOCAL_DIR + sFile, AppConstants.REMOTE_DIR + sFile)
    print "Completed SCP file transfer..."


#Use paramiko lib for SSH - http://jessenoller.com/blog/2009/02/05/ssh-programming-with-paramiko-completely-different
def deploy_file_now(sHost, sFile, sUser, sPassword):
    print "Starting SSH deployment..."
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(sHost, username=sUser, password=sPassword)
    (stdin, stdout, stderr) = ssh.exec_command("uptime")
    print stdout.readlines()
    (stdin, stdout, stderr) = ssh.exec_command("ls -l")
    print stdout.readlines()
    print "Completed SSH deployment..."


#Kick off the script
if __name__ == '__main__':
    main()
