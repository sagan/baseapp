package com.baseApp.controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/test")
public class RestSVC {

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg)
    {
        String output = "Jersey says: " + msg;
        return Response.status(200).entity(output).build();
    }

    @GET
    @Path("/bar/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Map getMap(@PathParam("param") String msg)
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("PARAM", msg);
        return map;
    }

    @GET
    //No @Path() defined, so its the root default
    @Produces(MediaType.APPLICATION_JSON)
    public Map getBaz()
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("path", "root");
        return map;
    }

}