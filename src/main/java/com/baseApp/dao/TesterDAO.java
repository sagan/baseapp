package com.baseApp.dao;

import java.sql.*;
import org.apache.log4j.Logger;
import java.util.*;
import java.util.Date;


public class TesterDAO {

    private static Logger _LOGGER = Logger.getLogger("TesterDAO");

    //ADD DB CRUD METHODS HERE
    public static Date selectDateTime(Connection connection)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "SELECT NOW()";

        try
        {
            preparedStatement = connection.prepareStatement("SELECT NOW()");
            preparedStatement.execute();

            resultSet = preparedStatement.getResultSet();
            resultSet.next();
            Date dtNow = resultSet.getTimestamp(1);

            return dtNow;
        }
        catch (Exception ex)
        {
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }


}
