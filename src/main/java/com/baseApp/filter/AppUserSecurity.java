package com.baseApp.filter;

import com.baseApp.security.AppUserPrincipal;
import com.baseApp.utility.AppDatabase;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

//This class is invoked based on the 'filter-mapping' in web.xml
public class AppUserSecurity implements Filter
{
    private static Logger _LOGGER = Logger.getLogger("AppUserSecurity");
    private static String _REMEMBER_ME = "RMID";
    private static boolean bOn = true;

    public void  init(FilterConfig config) throws ServletException{
        // Get init parameter
        String sOn = config.getInitParameter("on");
        if (sOn.equals("false"))
        {
            bOn = false;
        }
    }

    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        if (bOn)
        {
            //Dont authenticate on the home, index, or login requests
            if(true)
            {
                _LOGGER.info("AUTH check is SKIPPED for open endpoint/pages.");
            }
            else
            {
                //Cast the request/response to something useful
                HttpServletRequest httpReq = (HttpServletRequest)request;
                HttpServletResponse httpRes = (HttpServletResponse)response;

                //Get the AppUser for the current request
                AppUserPrincipal appUser = getCurrentUser(httpReq, httpRes);
                _LOGGER.info("AUTH check RESULT - AppUser: " + appUser);

                if (appUser == null)
                {
                    throw new SecurityException("Current AppUser is null");
                }
                else
                {
                    setLoggedInAppUser(httpReq, appUser);
                }
            }
        }
        else
        {
            _LOGGER.info("AUTH check is OFF.");
        }

        // Pass request back down the filter chain
        chain.doFilter(request,response);
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }

    ///////////End of Filter methods, Public API

    public static AppUserPrincipal getLoggedInAppUser(HttpServletRequest request)
    {
        AppUserPrincipal appUser = (AppUserPrincipal)request.getAttribute("LOGGED_IN_USER");
        return appUser;
    }

    public static void login(AppUserPrincipal appUser, HttpServletRequest request, HttpServletResponse response)
    {
        if (appUser != null)
        {
            //Give the user 10 years to not have to re-login
            String encryptedUID = AppUserSecurity.encryptToken(String.valueOf(appUser.get_id()));
            AppUserSecurity.setCookie(response, _REMEMBER_ME, encryptedUID, 3650);
        }
        else
        {
            AppUserSecurity.logout(request, response);
        }
    }

    public static void logout(HttpServletRequest request, HttpServletResponse response)
    {
        //Expire the RememberMe cookie
        AppUserSecurity.setCookie(response, _REMEMBER_ME, "null", 0);
    }

    ///////////////Private methods

    private static void setLoggedInAppUser(HttpServletRequest request, AppUserPrincipal appUser)
    {
        request.setAttribute("LOGGED_IN_USER", appUser);
    }

    private static AppUserPrincipal getCurrentUser(HttpServletRequest request, HttpServletResponse response)
    {
        AppUserPrincipal appUser = null;

        //Use the RememberMe cookie to pull the USER from the DB
        Map<String, Cookie> hmCookies = AppUserSecurity.getCookies(request);
        Cookie cookie = hmCookies.get(_REMEMBER_ME);

        //If the cookie is not found, assume no logged in user
        if (cookie != null)
        {
            String encryptedUID = cookie.getValue();
            String decryptedUID = AppUserSecurity.decryptToken(encryptedUID);
            appUser = selectUserByID(decryptedUID);

            //If the account has been set to INACTIVE, after they logged in, stop per request
            if (appUser != null && !appUser.isAccountActive())
            {
                appUser = null;
                AppUserSecurity.logout(request, response);
            }
        }

        return appUser;
    }

    ///////////////Private Helpers

    //Get all the cookies from the HTTPRequest; this should be a normal Servlet API!
    private static Map<String, Cookie> getCookies(HttpServletRequest request)
    {
        Map<String, Cookie> mapCookies = new HashMap<String, Cookie>();
        Cookie[] cookies = request.getCookies();

        //When there are no cookies, this [] evaluates to null
        if (cookies != null)
        {
            for(int i = 0; i < cookies.length; i++)
            {
                Cookie cookie = cookies[i];
                mapCookies.put(cookie.getName(), cookie);
            }
        }

        return mapCookies;
    }

    //Use to set a cookie back to the client, simple API
    private static void setCookie(HttpServletResponse response, String key, String value, int iDaysExpire)
    {
        Cookie cookie = new Cookie(key, value);
        cookie.setMaxAge(iDaysExpire*24*60*60);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    private static AppUserPrincipal selectUserByID(String sUID)
    {
        //Get a DB connection, do the work, and close it
        java.sql.Connection connection = AppDatabase.getConnection();
        try
        {
            int anID = Integer.parseInt(sUID);
            _LOGGER.info("AUTH DB lookup is not implemented yet...");
            //AppUserPrincipal appUser = UserPrincipalDAO.selectUserByID(connection, anID);
            //return appUser;
            return null;
        }
        catch (Exception ex)
        {
            //In case the cookie is bad, or blank, assume no user
            return null;
        }
        finally
        {
            AppDatabase.close(connection, false);
        }
    }

    //////////////Encryption methods

    //This is a 16 byte secret key for AES
    private static final byte[] key = {
            0x53, 0x52, 0x4a, 0x56, 0x53, 0x47, 0x4b, 0x53, 0x53, 0x53, 0x53, 0x53, 0x33, 0x33, 0x36, 0x30
    };

    private static String encryptToken(String aToken)
    {
        String encryptedToken = null;

        try
        {
            //The date is like salt for encrypting, and is all ignored on decryption
            Date dtNow = new Date();
            //Assumption is what we are encrypting does NOT have this '|' delimiter
            String sToken = UUID.randomUUID().toString() + "|" + aToken + "|" + dtNow.toString() + "|" + dtNow.getTime();

            //Do the actual encryption
            Cipher c = Cipher.getInstance("AES");
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            c.init(Cipher.ENCRYPT_MODE, secretKey);
            encryptedToken = Base64.encodeBase64String(c.doFinal(sToken.getBytes()));
        }
        catch (Exception ex)
        {
            _LOGGER.error("Error trying to encrypt token");
            _LOGGER.error(ex.getMessage());
        }
        return encryptedToken;
    }

    private static String decryptToken(String encryptedToken)
    {
        String originalToken = null;

        try
        {
            //Do the actual decryption
            Cipher c = Cipher.getInstance("AES");
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            c.init(Cipher.DECRYPT_MODE, secretKey);
            String decryptedToken = new String(c.doFinal(Base64.decodeBase64(encryptedToken)));

            //The second token is the UID, the rest is the date padding for uniqueness
            StringTokenizer st = new StringTokenizer(decryptedToken, "|");
            originalToken = st.nextToken();
            originalToken = st.nextToken();
        }
        catch (Exception ex)
        {
            _LOGGER.error("Error trying to decrypt token");
            _LOGGER.error(ex.getMessage());
        }

        //Handle the null case
        if ("null".equals(originalToken))
        {
            return null;
        }
        else
        {
            return originalToken;
        }
    }
}