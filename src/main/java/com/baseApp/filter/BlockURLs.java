package com.baseApp.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

// Requests that match the web.xml 'filter-mapping' url-pattern, will come here
// If anyone tries to certain URLs (i.e. test.jsp), we will kick them back to the homepage
public class BlockURLs implements Filter
{
    //Default to blocking mode, fail-safe
    private static boolean bOn = true;

    public void  init(FilterConfig config) throws ServletException{
        // Get init parameter
        String sOn = config.getInitParameter("on");
        if (sOn.equals("false"))
        {
            bOn = false;
        }
    }

    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws java.io.IOException, ServletException
    {
        if (bOn)
        {
            //This is the default homepage of the app
            request.getRequestDispatcher("/").forward(request, response);
        }
        else
        {
            //Continue to filter like a good citizen
            chain.doFilter(request,response);
        }
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }
}