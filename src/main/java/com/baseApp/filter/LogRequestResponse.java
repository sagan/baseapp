package com.baseApp.filter;

import com.baseApp.security.AppUserPrincipal;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

//Log all the request-response in one place
public class LogRequestResponse implements Filter
{
    private static Logger _LOGGER = Logger.getLogger("LogRequestResponse");

    public void  init(FilterConfig config) throws ServletException{
        // Get init parameter
        //String testParam = config.getInitParameter("test-param");
    }

    //The iPhone ios6 seems to cache AJAX post calls, so we are forcing new requests for all
    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        //Cast the request/response to something useful
        HttpServletRequest httpReq = (HttpServletRequest)request;
        HttpServletResponse httpRes = (HttpServletResponse)response;

        //Log the start
        logRequestStart(httpReq, AppUserSecurity.getLoggedInAppUser(httpReq));
        long lTimeStart = System.currentTimeMillis();

        // Pass request back down the filter chain to do the real work
        chain.doFilter(request,response);

        //Log the end
        long lTimeEnd = System.currentTimeMillis();
        logRequestEnd(httpReq, AppUserSecurity.getLoggedInAppUser(httpReq), lTimeEnd-lTimeStart);
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }

    private void logRequestStart(HttpServletRequest request, AppUserPrincipal dtoUser)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("START").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString());
            _LOGGER.info(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    private void logRequestEnd(HttpServletRequest request, AppUserPrincipal dtoUser, long lTimeMS)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("200OK").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString()).append(", ");
            sbMsg.append("TIME_MS=").append(lTimeMS);
            _LOGGER.info(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    private void logError(HttpServletRequest request, AppUserPrincipal dtoUser, Exception anEX)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("500EX").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString()).append(", ");
            sbMsg.append("EX_MSG=").append("\"").append(anEX.getMessage()).append("\"");
            _LOGGER.error(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    ///////////////// Generic Logging for App

    protected String formatAppEvent(AppUserPrincipal dtoUser, String sEvent, Map<String, String> mapEventArgs)
    {
        StringBuilder sbMsg = new StringBuilder();

        try
        {
            sbMsg.append("APP_EVENT=").append(sEvent).append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");

            //Sometimes there isnt too much to report...
            if (mapEventArgs != null)
            {
                for (Map.Entry<String, String> entry : mapEventArgs.entrySet()) {
                    sbMsg.append(entry.getKey()).append("=").append(entry.getValue()).append(", ");
                }
            }

            //So the last KV has no comma
            sbMsg.append("EOM=").append("EOL");
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }

        return sbMsg.toString();
    }
}