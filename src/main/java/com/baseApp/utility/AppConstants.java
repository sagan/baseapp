package com.baseApp.utility;

import org.apache.commons.configuration.*;
import org.apache.log4j.Logger;

/**
 * Use for accessing the runtime.config properties
 */
public class AppConstants {
    private static Logger _LOGGER = Logger.getLogger("AppConstants");

    //Add your application constants here
    public static String _APP_VERSION;
    public static String _DB_HOST;
    public static String _DB_NAME;
    public static String _DB_USER;
    public static String _DB_PASS;

    static
    {
        try
        {
            _LOGGER.info("Loading: runtime.config");
            Configuration config = new PropertiesConfiguration("runtime.config");

            _APP_VERSION = config.getString("app_version");

            _DB_HOST = config.getString("db_host");
            _DB_NAME = config.getString("db_name");
            _DB_USER = config.getString("db_user");
            _DB_PASS = config.getString("db_pass");

            _LOGGER.info("Completed: runtime.config");
        }
        catch (Exception ex)
        {
            _LOGGER.error("Could not load file: runtime.config");
            _LOGGER.error(ex.getMessage());
        }
    }

}
