package com.baseApp.utility;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import java.util.*;
import java.text.*;

/**
 * The UI = UserInterface
 * Provide utility functions for rendering HTML, JSP, etc
 */
public class UI {

    private static Logger _LOGGER = Logger.getLogger("UI");

    //If the string has a single quote in it, it may screw up your JS, depending on how you use it (i.e. ng-init="scopvar='foo's'")
    public static String printAndEscapeForJS(String aString)
    {
        if (aString == null)
        {
            return "";
        }
        else
        {
            String escapedString = StringEscapeUtils.escapeEcmaScript(aString);
            return escapedString;
        }
    }

    //Prevents use of quotes and ancle brackets to disrupt the HTML
    public static String printAndEscapeForHTML(String aString)
    {
        if (aString == null)
        {
            return "";
        }
        else
        {
            String escapedString = StringEscapeUtils.escapeHtml4(aString);
            return escapedString;
        }
    }

    public static boolean isNullOrEmpty(String aString)
    {
        if (aString == null || "".equals(aString.trim()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean startsWithHttp(String sURL)
    {
        if (isNullOrEmpty(sURL))
        {
            return false;
        }
        else if (sURL.startsWith("http://"))
        {
            return true;
        }
        else if (sURL.startsWith("https://"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
