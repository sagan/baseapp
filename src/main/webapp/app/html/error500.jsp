<%@ page isErrorPage="true" import="java.util.*, com.baseApp.utility.*" %>
<!doctype html>
<html>

<head>
    <title>BaseApp</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>

<body>

    <h1>The BaseApp error 500</h1>
    <p>There was an unhandled exception!</p>
    <p>Back to the <a href="/">home</a> page</p>
    <br>

    <div>
        <p>Message: <%= exception.getMessage() %></p>
        <p>Method: <%= request.getMethod() %></p>
        <p>ReqURI: <%= request.getAttribute("javax.servlet.error.request_uri") %></p>
        <p>QueryString: <%= request.getQueryString() %></p>
    </div>

</body>
</html>