<%@ page import="java.util.*, com.baseApp.utility.*" %>
<%
    //Do nothing for now
%>
<!doctype html>
<html>

  <head>
    <title>Test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
  </head>

<body>
    <h1>BaseApp, Hello World!</h1>
    <p>Version:<%=AppConstants._APP_VERSION%></p>

    <p>How about a <a href="/app/html/login.jsp">login</a> page</p>
    <p>How about a <a href="/app/html/dashboard.jsp">dashboard</a> page</p>
    <p>How about an <a href="/ajax/test/foo">AJAX</a> String</p>
    <p>How about an <a href="/ajax/test/bar/hi">AJAX</a> JSON</p>
    <p>How about an <a href="/ajax/test/">AJAX</a> root</p>
</body>

</html>