<%@ page import="java.util.*, com.baseApp.utility.*" %>
<!doctype html>
<html>
  
  <head>
    <title>BaseApp</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/lib/css/bootstrap-3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/lib/css/bootstrap-3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/lib/css/font-awesome-4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/app/css/main.css?v=<%=AppConstants._APP_VERSION%>">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/lib/js/jquery_1.11.1/jquery.min.js"></script>
    <script src="/lib/js/angular_1.2.20/angular.min.js"></script>
    <script src="/lib/css/bootstrap-3.2.0/js/bootstrap.min.js"></script>
    <script src="/app/js/index.js?v=<%=AppConstants._APP_VERSION%>"></script>
  </head>
  
  <body>

  <h1>The BaseApp is up</h1>
  <p>Version is: <%=AppConstants._APP_VERSION%></p>
  <p>How about a <a href="/app/html/test.jsp">test</a> page</p>

  </body>
</html>