package rest;

import static com.jayway.restassured.RestAssured.expect;
import org.junit.*;
import com.jayway.restassured.RestAssured;

public class RestSVC extends SuperRest {

    @BeforeClass
    public static void oneTimeSetUp() throws Exception
    {
        SuperRest.oneTimeSetUp();
        RestAssured.basePath = "/ajax/test";
    }

    @Test
    public void checkRootPage()
    {
        expect()
            .statusCode( 200 )
        .when()
            .get ( "/" );
    }

}
