package rest;

import com.jayway.restassured.RestAssured;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.*;
import java.net.ServerSocket;

//https://code.google.com/p/rest-assured/wiki/Usage
public class SuperRest {
    private static Logger _LOGGER = Logger.getLogger("SuperRest");
    private static Server webServer;

    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if (webServer == null)
        {
            //Find any open port, and start Jetty on it
            ServerSocket socket = new ServerSocket(0);
            int localPort = socket.getLocalPort();
            socket.close();
            webServer = new Server(localPort);

            //http://wiki.eclipse.org/Jetty/Tutorial/Embedding_Jetty#Setting_a_Web_Application_Context
            if (!webServer.isRunning()) {
                _LOGGER.info("Server port number is: " + localPort);
                WebAppContext context = new WebAppContext();
                context.setDescriptor("/WEB-INF/web.xml");
                context.setResourceBase("src/main/webapp");
                context.setContextPath("/");
                context.setParentLoaderPriority(true);
                webServer.setHandler(context);
                webServer.start();
            }

            //Tell RestAssured client where to find the webServer
            RestAssured.baseURI  = "http://localhost";
            RestAssured.port = localPort;
            RestAssured.basePath = "/ajax";
        }
    }

    @AfterClass
    public static void oneTimeTearDown() throws Exception {
        // No need to stop the webServer for each Test class
        // webServer.stop();
        //RestAssured.requestSpecification = null;
    }

}
