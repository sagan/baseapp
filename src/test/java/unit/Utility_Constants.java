package unit;

import org.junit.*;
import static org.junit.Assert.*;
import com.baseApp.utility.AppConstants;

public class Utility_Constants {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }


    @Test
    public void checkVersion()
    {
        assertTrue(AppConstants._APP_VERSION != null);
    }


}
