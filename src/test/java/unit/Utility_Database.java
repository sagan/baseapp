package unit;

import com.baseApp.utility.AppDatabase;
import com.baseApp.dao.TesterDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class Utility_Database {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }


    @Test
    public void checkDbTime()
    {
        Connection conn = null;

        try
        {
            //Get a DB connection from the pool
            conn = AppDatabase.getConnection();
            Date dtNow = TesterDAO.selectDateTime(conn);

            //Check the results
            assertTrue(dtNow != null);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
        finally
        {
            //Release the DB connection
            AppDatabase.close(conn, false);
        }
    }


}
