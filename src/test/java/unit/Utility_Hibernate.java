package unit;

import com.baseApp.utility.*;
import com.baseApp.dto.*;
import org.hibernate.Session;
import java.util.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Utility_Hibernate {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        HibernateUtil.getSessionFactory();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
        HibernateUtil.closeSessionFactory();
    }


    @Test
    public void checkInsert()
    {
        //http://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html_single/
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        EventDTO theEvent = new EventDTO();
        theEvent.setTitle("My Event");
        theEvent.setDate(new Date());
        session.save(theEvent);

        session.getTransaction().commit();
        assertTrue(true);
    }

    @Test
    public void checkSelect()
    {
        //http://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html_single/
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        EventDTO theEvent = (EventDTO)session.get(EventDTO.class, 1L);

        session.getTransaction().rollback();
        assertTrue(true);
    }

    @Test
    public void checkUpdate()
    {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        EventDTO theEvent = (EventDTO)session.get(EventDTO.class, 1L);
        theEvent.setTitle("My Event: " + (new Date()).getTime());
        session.update(theEvent);

        session.getTransaction().commit();
        assertTrue(true);
    }


}
